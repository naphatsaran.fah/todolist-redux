import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Card,
  InputGroup,
  Button,
  FormControl,
  FormGroup,
} from "react-bootstrap";
import styles from "./Add.module.css";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import moment from "moment";
import todoSlice, { addTodo, deleteTodo, selectTodoList } from "../slices/todoSlice";
import { useSelector, useDispatch } from "react-redux";
import ListTodos from "./ListTodos";


function Add() {
  const [title, setTitle] = useState("");
  const [details, setDetails] = useState("");
  const data = useSelector(selectTodoList);

  const [titleErr, setTitleErr] = useState("");
  const [detailsErr, setDetailsErr] = useState("");
  const [InfoErr, setInfoErr] = useState("");

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleAdd = () => {
    if (!title && !details) {
      setTitleErr("pless fill the title");
      setDetailsErr("pless fill the details");
    } else if (title.length > 20 && details.length >= 3) {
      setTitleErr("The length must be less than 20 characters.");
      setDetailsErr("");
      setInfoErr("");
    } else if (details.length > 80 && title.length >= 3) {
      setTitleErr("");
      setDetailsErr("The length must be less than 80 characters.");
      setInfoErr("");
    } else if (title.length < 3 && !details) {
      setTitleErr("title  must contain at least 3 characters");
      setDetailsErr("");
    } else if (title.length >= 3 && !details) {
      setInfoErr("Please fill out the information completely.");
      setTitleErr("");
      setDetailsErr("");
    } else if (!title || !details) {
      setInfoErr("Please fill out the information completely.");
    } else if (title.length < 3) {
      setTitleErr("title  must contain at least 3 characters");
    } else if (title.length >= 3 && details.length < 3) {
      setTitleErr("");
      setDetailsErr("details  must contain at least 3 characters");
      setInfoErr("");
    } else if (details.length < 3) {
      setDetailsErr("details  must contain at least 3 characters");
    } else if (details.length >= 3 && title.length < 3) {
      setDetailsErr("");
      setTitleErr("title  must contain at least 3 characters");
    } else {
      
      setTitleErr("");
      setDetailsErr("");
      setInfoErr("");

      console.log(`inputTitle ${title}`);
      console.log(`inputDetails ${details}`);

      dispatch(
        
        addTodo({
          title: title,
          details: details,
          addDateTime: moment().format("lll"),
          id: Date.now(),
        })

      );
      // console.log(data);
    }
  };

  const handleEdit = (e) => {
    e.preventDefault();
    navigate(`/edit/`);
  };

  const handleDelete = (e) => {


  };

  return (
    <div>
      <Card className={styles.card}>
        <Card.Body className={styles.test}>
          <Card.Title>To do List</Card.Title>

          <p className={styles.err} value={InfoErr}>
            {InfoErr}
          </p>
          <InputGroup className={styles.titleInput}>
            <InputGroup.Text id="inputGroup-sizing-default">
              Title
            </InputGroup.Text>
            <FormControl
              placeholder="Please enter Title"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />
          </InputGroup>
          <p className={styles.err} value={titleErr}>
            {titleErr}
          </p>

          <InputGroup className={styles.detailInput}>
            <InputGroup.Text>Details</InputGroup.Text>
            <FormControl
              value={details}
              onChange={(e) => setDetails(e.target.value)}
              as="textarea"
              aria-label="With textarea"
              placeholder="Please enter Details"
            />
          </InputGroup>
          <p className={styles.err} value={detailsErr}>
            {detailsErr}
          </p>

          <FormGroup>
            <Button
              className={styles.btnAdd}
              variant="success"
              onClick={() => handleAdd()}
            >
              Add
            </Button>
          </FormGroup>
        </Card.Body>
      </Card>


      <FormGroup>

      {data.map((item, index) => {
                return (
                  <ListTodos
                    title={item.title}
                    details={item.details}
                    addDateTime={item.addDateTime}
                    key={index}
                  />
                );
              })}
                   
      </FormGroup>

      
    </div>
  );
}

export default Add;
