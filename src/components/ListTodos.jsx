import React from "react";
import styles from "./ListTodos.module.css";
import { Button, Card, Row } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import * as Icons from "react-icons/fa";
import handleDelete from '../components/Add'
import handleEdit from '../components/Add'


const ListTodos = ({ title, details, addDateTime }) => {


  
  return (
    <Card className={styles.showItem}>
      Title : {title}
      <br /> Details : {details}
      <br /> AddDateTime : {addDateTime}
      <Row className={styles.icons}>
        <Icons.FaEdit className={styles.iconEdit} onClick={handleEdit}/>
        <Icons.FaTrash className={styles.iconDelete} onClick={handleDelete} />
      </Row>
    </Card>
  );
};

export default ListTodos;
