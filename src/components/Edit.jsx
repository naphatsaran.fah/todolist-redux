import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {Card,InputGroup,Button,FormControl, FormGroup,} from "react-bootstrap";
import styles from "./Add.module.css";

function Edit () {
    return (
        <div>
        <Card className={styles.card}>
          <Card.Body className={styles.test}>
            <Card.Title>Edit</Card.Title>
  
  
            <InputGroup className={styles.titleInput}>
              <InputGroup.Text id="inputGroup-sizing-default" type="text" >
                Title
              </InputGroup.Text>
              <FormControl placeholder="Please enter Title" />
            </InputGroup>
  

           
          <InputGroup className={styles.detailInput}>
            <InputGroup.Text>Details</InputGroup.Text>
            <FormControl
              as="textarea"
              aria-label="With textarea"
              placeholder="Please enter Details"
            />
          </InputGroup>
          
            <FormGroup>
            <Button variant="success">Submit</Button>
            </FormGroup>
  
          </Card.Body>
        </Card>
      </div>
    )
}


export default Edit 